# Instructions

- Clone this repository

    - Clone into a private repo

    - Permit the user [OnceUponALoop](https://gitlab.com/OnceUponALoop) access.

- Implement the requirements outlined below

- Submit your repo address to CodeBytes at the prompt


# Requirements
## Vagrant & Ansible

Create a [Vagrantfile](https://www.vagrantup.com/docs/vagrantfile/) that uses the [ansible_local](https://www.vagrantup.com/docs/provisioning/ansible_local.html) provisioner to define a machine configuration as follows:

### Base

- Box: CentOS 7 Base

- Hostname: `centos-eval`

- IP: Any RFC 1918 Address

- Provider: VirtualBox | HyperV | VMware Worksation

- Local Share: SMB | rsync

### Patch

- Install the latest available patches

- Install [`CEBA-2020:1982`](https://www.linuxcompatible.org/story/ceba20201982-centos-7-tzdata-bugfix-update/)
  
    This works in RHEL and not in CentOS - please explain why

    How would you do it if this was RHEL instead?

### Time Synchronization

- Install, enable & configure `chrony`

- User can define time source as a variable

- Support accepting a list of time sources and ensure the Jinja template is configured to support that.

- Default to North American NTP Pool if not defined

### Cockpit

- Install, enable & configure [cockpit](https://cockpit-project.org/)

- Restrict to listen to localhost

### Nginx Server

- Install, enable, & configure `nginx` webserver

- Configure nginx to proxy traffic to the cockpit instance

    - Proxy HTTP (80) & HTTPS (443) to cockpit instance listening on localhost

    - Ensure that Cockpit functionality is not affected, this can be verified by successfully logging into Cockpit.

## Gitlab Pipeline

A simple [Gitlab CI/CD](https://docs.gitlab.com/ee/ci/yaml/README.html) pipeline.

- `.gitlab-ci.yml` configuration file

- Stage: Check
    
    - Trigger on any branch

    - Trigger only when a [semver](https://semver.org/) tag is pushed

    - Validate vagrant

    - Lint Ansible

    - Validate Ansible  

# Submittal

## Files

- `README.md`

    - Overview of the functionality, choices made, and specific usage instructions.
    
    - Any additional notes, remarks, or clarifications.
    
    - Assume the target audience is a configuration management engineer.

- `Vagrantfile`

    A Vagrant definition file that uses the Ansible local provisioner and your choice of hypervisor to implement the functionality above.

- `share/`

    - Local share ( rsync | smb )

    - Ansible local collections, playbooks, roles, requirements.yml, etc...

- `.gitlab-ci.yml`

    - Gitlab CI configuration file



