# DevOps Skill Evaluation

Refer to [INSTRUCTIONS](INSTRUCTIONS.md) before starting.

## Usage Instructions

- Usage instrictions

- Target is an end-user (CM Engineer)

## Notes

- Any notes or useful information 

- Target is an end-user (CM Engineer)

## Considerations, Issues, etc...

- Any other information. 

- Targeted at the interviewer.
